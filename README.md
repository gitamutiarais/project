# Project

Aplikasi Online Shop


Aplikasi ini merupakan aplikasi online shop / toko online yang dapat membeli berbagai macam barang. Pada aplikasi terdapat menu Produk, menu Pesanan, menu Admin, menu tentang saya dan menu keranjang.  
Aplikasi ini di buat untuk memenuhi tugas Final Project dari bootcamp Reactnative Sanbarcode batch Juli 2020. 


cara penggunaan aplikasi :

- git clone Repository
- cd Repository
- npm install 
- npm start



Tool yang di gunakan :

- Expo CLI 
- Terhubung dengan API 
- React Navigation 5
- Redux
- Vscode
- Figma
- GitLab
- Emulator


Link gitLab : https://gitlab.com/gitamutiarais/final-project
Link Mockup : https://www.figma.com/file/PhqsKOECaAMyTRO60WvxOx/Mockup?node-id=0%3A1
Link project : https://drive.google.com/file/d/1VvJPLsyW1Tpwm3iRzrS0qlUl64T_JXSB/view?usp=sharing
Link Video : https://drive.google.com/file/d/1jdtmC-j29lfrnHpKoNT004-beRwOi1_V/view?usp=sharing